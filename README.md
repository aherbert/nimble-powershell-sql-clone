# README

This script is designed to clone a Nimble Storage volume from a source server to a target server via iSCSI or FC. The script will query the sorce server for the source database name to find the underlying source

This script requires the sqlps Powershell module. It can be installed by installing the following [Microsoft SQL Server Feature Packs](https://www.microsoft.com/en-us/download/details.aspx?id=52676) in order.

 1. SQLSysClrTypes
 1. SharedManagementObjects
 1. PowerShellTools

* Be sure to choose the correct platform (x86, x64) for the OS install

The hosts will need to have PS remoting enabled by running ```Enable-PSRemoting -Force```. Also remember to set the execution policy on the host that will run this script with ```Set-ExecutionPolicy Bypass -Force```.